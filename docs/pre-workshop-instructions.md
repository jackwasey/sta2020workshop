---
output:
  pdf_document: default
  html_document: default
---
# STA 2020 R Workshop

Jan 17, 2019

#### Jack Wasey BMBCh MA MSci MSc

 - Children's Hospital of Philadelphia
 - waseyj@chop.edu

#### Nina Alfaro MS

 - Children's Hospital of Philadelphia
 - alfarom@chop.edu


We're looking forward to teaching or refreshing your memory about how powerful R can be. Please follow the instructions below to prepare. Feel free to email us both with any set-up problems.

## Before the workshop

- Charge your laptop fully

- [Install R](https://player.vimeo.com/video/203516510)

    https://cran.rstudio.com/

- [Install RStudio](https://player.vimeo.com/video/203516968)
  
  https://rstudio.com/products/rstudio/download/

- Bookmark the following:
  
  - http://www.rdocumentation.org/

- Open RStudio, and in the R command window:
  
  - `install.packages(c("learnr", "icd", "ggplot2", "automl"))`

Now, depending on your experience, we recommend at least scanning through a few pages of this [tutorial](https://www.computerworld.com/article/2497143):

https://www.computerworld.com/article/2497143

There are many such tutorials, and this one stands out for covering the basics well.
